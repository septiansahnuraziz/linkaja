package config

import (
	"fmt"
	"log"
	"os"

	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
)


type DB struct {
	SQL *sqlx.DB
}

var dbConn = &DB{}

var dropDB = "DROP DATABASE db_linkaja;"
var createrDB = "CREATE DATABASE db_linkaja"

var schema = `CREATE TABLE IF NOT EXISTS "customer"
				(
					customer_number integer NOT NULL,
					name character varying(155) COLLATE pg_catalog."default",
					CONSTRAINT customer_pkey PRIMARY KEY (customer_number)
				);

				CREATE TABLE IF NOT EXISTS "account"
				(
					account_number integer NOT NULL,
					customer_number integer,
					balance integer,
					CONSTRAINT account_pkey PRIMARY KEY (account_number)
				)`

var dropTable = "DROP TABLE customer, account;"
var insertData = "INSERT INTO public.customer(customer_number, name) VALUES (1001, 'Boob Martin'), (1002, 'Linus Torvalds'); INSERT INTO public.account(account_number, customer_number, balance)VALUES (555001, 1001, 10000), (555002, 1002, 15000); "

func ConnectSQL() (*DB, error) {
	if err := godotenv.Load(); err != nil {
		log.Fatal("error loading file .env")
	}

	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbPass := os.Getenv("DB_PASS")
	dbUser := os.Getenv("DB_USER")
	dbName := os.Getenv("DB_NAME")

	dbUrl := fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=disable", dbUser, dbPass, dbHost, dbPort, dbName)
	url := fmt.Sprintf("postgres://%v:%v@%v:%v?sslmode=disable", dbUser, dbPass, dbHost, dbPort)

	dbConn1, err := sqlx.Connect("postgres", url)
    if err != nil {
        log.Fatalln(err)
    }
	dbConn1.MustExec(dropDB)
	dbConn1.MustExec(createrDB)
	dbConn1.MustExec(dropTable)
	dbConn1.MustExec(schema)
	dbConn1.MustExec(insertData)

	fmt.Println("Datasource ", dbUrl)
	db, err := sqlx.Open("postgres", dbUrl)

	if err != nil {
		log.Fatal(err.Error())
	}
	
	

	db.SetMaxIdleConns(5)
	db.SetMaxOpenConns(5)
	db.SetConnMaxIdleTime(5)

	dbConn.SQL = db
	return dbConn, nil

}