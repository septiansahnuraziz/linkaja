package controllers

import (
	"linkaja-services/config"
	"linkaja-services/models"
	"linkaja-services/repository"
	"linkaja-services/repository/impl"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)


type Linkaja struct {
	LinkajaRepo repository.Customer
}

func NewLinkajaController(db *config.DB) Linkaja {
	return Linkaja{
		LinkajaRepo: impl.NewSQLLinkaja(db.SQL),
	}
}

//list route/endpoints
func (linkAja *Linkaja) Route(app *gin.Engine) {
	app.GET("/account/:account_number", linkAja.CheckSaldo)
	app.POST("/account/:from_account_number/transfer", linkAja.Transfer)
}

//fungsi untuk cek saldo
func (repo *Linkaja) CheckSaldo(c *gin.Context) {
	accountNumber := c.Param("account_number")

	//pengecekan apakah parameter accountNumber itu ada atau tidak
	if len(accountNumber) == 0 {
		c.JSON(http.StatusBadRequest, gin.H{
			"message" : "account_number is required",
		})
		return
	}

	//menkonversi parameter accountNumber menjadi integer
	accntNum, _ := strconv.Atoi(accountNumber)

	payload, err := repo.LinkajaRepo.CheckSaldo(accntNum)	
	if err != nil {
		//return error respos, jika terjadi error/data tidak ada didalam proses query
		c.JSON(http.StatusBadRequest, gin.H{
			"message" : err.Error(),
		})
		return
	}

	//return payload/data hasil query dari funsi CheckSaldo
	 c.JSON(201, payload)
}

func (repo *Linkaja) Transfer(c *gin.Context) {
	
	var transfer models.Transfer

	fromAccountNumber := c.Param("from_account_number")
	
	//pengecekan apakah parameter fromAccountNumber itu ada atau tidak
	if len(fromAccountNumber) == 0 {
		c.JSON(http.StatusBadRequest, gin.H{
			"message" : "from_account_number is required",
		})
		return
	}

	//binding body raw dengan model transfer yang sudah di buat
	if err := c.BindJSON(&transfer); err != nil {

		//jika hasil binding tidak sesuai dengan model transfer, maka akan return error
		c.JSON(http.StatusBadRequest, gin.H{
			"message" : err.Error(),
		})
		return
	}

	//menkonversi parameter accountNumber menjadi integer
	fromAccntNum, _ := strconv.Atoi(fromAccountNumber)

	//proses query
	if err := repo.LinkajaRepo.Transfer(fromAccntNum, transfer); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message" : err.Error(),
		})
		return
	}

	c.JSON(204, http.StatusNoContent)
}