module linkaja-services

go 1.15

require (
	github.com/gin-gonic/gin v1.7.4
	github.com/jmoiron/sqlx v1.3.4
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.4
	github.com/onsi/ginkgo v1.16.5
	golang.org/x/text v0.3.3
)
