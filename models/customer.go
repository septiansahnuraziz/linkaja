package models

type CheckSaldo struct {
	Account_Number int `json:"account_number"`
	Customer_Name string `json:"customer_name"`
	Balance int `json:"balance"`
}

type Transfer struct {
	ToAccountNumber int `json:"to_account_number"`
	Ammount int `json:"amount"`
}