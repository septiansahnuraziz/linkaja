package impl

import (
	"errors"
	"fmt"
	"linkaja-services/models"
	repo "linkaja-services/repository"
	"log"

	"github.com/jmoiron/sqlx"
)

type mySQLLinkaja struct {
	Conn *sqlx.DB
}

func NewSQLLinkaja(Conn *sqlx.DB) repo.Customer {
	return &mySQLLinkaja{
		Conn: Conn,
	}
}

func (m *mySQLLinkaja) CheckSaldo(accountNumber int) (models.CheckSaldo, error) {
	var result models.CheckSaldo
	var accountNum int

	//pengecekan akun
	if err := m.Conn.Get(&accountNum, `SELECT account_number FROM "account" WHERE account_number =$1`, accountNumber); err != nil {
		log.Panicln(err.Error())
		return result, errors.New("Akun tidak ditemukan")
	}

	//menampilkan data account
	query := fmt.Sprintf(`SELECT t.account_number,
			  	cst.name as customer_name,
			  	t.balance
			  FROM "account" t
			  	LEFT JOIN customer cst
			  		ON cst.customer_number = t.customer_number
			  WHERE  t.account_number = %d`, accountNumber)
	if err := m.Conn.Get(&result, query); err != nil {
		return result, err
	}

	return result, nil
}

func (m *mySQLLinkaja) Transfer(fromAccntNum int, trf models.Transfer) (error) {
	var balance int

	//begin Transaction Query
	tx := m.Conn.MustBegin()

	//Get balance value dari account pengirim
	if err := tx.Get(&balance, `SELECT balance FROM public.account WHERE account_number = $1`, fromAccntNum); err != nil {
		
		//jika gagal semua query batal di eksekusi
		tx.Rollback()
		return err
	}

	//Check saldo account pengeirim
	if balance < trf.Ammount {
		return errors.New("Saldo tidak cukup")
	}

	//mengkalkulasi saldo account penerima
	saldo_account_rcv := balance + trf.Ammount
	 _, errRcv := tx.Exec(`UPDATE account SET public.balance =$1 WHERE account_number =$2`, saldo_account_rcv, trf.ToAccountNumber); 
	
	 if errRcv != nil {

		 //jika gagal semua query batal di eksekusi
		 tx.Rollback()
		return errRcv
	}

	//mengkalkulasi sisa saldo pengirim
	saldo_account_trf := balance - trf.Ammount
	 _, errTrf := tx.Exec(`UPDATE account SET public.balance =$1 WHERE account_number =$2`, saldo_account_trf, fromAccntNum); 
	
	 if errTrf != nil {

		 //jika gagal semua query batal di eksekusi
		 tx.Rollback()
		return errTrf
	}

	//commit semua semua query yg ada di dalam transaksi query
	tx.Commit()

	return nil
}