package repository

import "linkaja-services/models"

type Customer interface {
	CheckSaldo(accountNumber int) (models.CheckSaldo, error)
	Transfer(accountNumber int, transfer models.Transfer) (error)
}