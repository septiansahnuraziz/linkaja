package main

import (
	"linkaja-services/config"
	"linkaja-services/controllers"
	"log"

	"github.com/gin-gonic/gin"
)

func main() {

	//define koneksi
	conn, errConn := config.ConnectSQL()
	if errConn != nil {
		log.Fatal(errConn)
	}

	//define controller
	linkAjaController := controllers.NewLinkajaController(conn)

	//define app gin
	app := gin.Default()
	
	//memanggil fungsi route yg ada didalam controller
	linkAjaController.Route(app)

	//meng serve di port 8000
	if err := app.Run(":8002"); err != nil {
		log.Fatal(err)
	}
}